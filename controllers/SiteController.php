<?php

namespace app\controllers;

use Yii;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Pdffile;
use Mpdf\Mpdf;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionPdffile()
    {
        $models = [new Pdffile];
        for($i=1;$i<count(Yii::$app->request->post('Pdffile',[]));$i++)
        {
            $models[]=new Pdffile();
        }
        if (Model::loadMultiple($models,Yii::$app->request->post()) && Model::validateMultiple($models)){
            // valid data received in $model
            // do something meaningful here about $model ...
            foreach($models as  $key=>$model)
            {
                $mpdf = new mPDF;
                $img=$model->actionPdf();
                $mpdf->WriteHTML($img);
                $random=rand(1,2000);
                $mpdf->Output("pdf".$key."-".$random.".pdf","F"); //saved in web folder
                unset($mpdf);
                unset($img);
            }
            \Yii::$app->session->setFlash('flashMessage', 'Your Pdf Files Are Generated Successfully!!!');
            return $this->render('pdffile', ['models' => $models]);
        } else {
            // either the page is initially displayed or there is some validation error
            return $this->render('pdffile', ['models' => $models]);
        }
    }
}
