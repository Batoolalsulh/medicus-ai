<?php
namespace app\models;
use Mpdf\Mpdf;
use yii\base\Model;
use Da\QrCode\QrCode;

class Pdffile extends Model
{
    public $QRCodeNumbers;
    public $QRColor;
    public $QRPrefix;

    public function rules()
    {
        return [
            [['QRCodeNumbers', 'QRColor','QRPrefix'], 'required'],
            ['QRCodeNumbers', 'integer'],
        ];
    }
    public function QRCodesGenerater(){
        $qrcodes=array();
        $split_hex_color = str_split( $this->QRColor, 2 );
        $rgb1 = hexdec( $split_hex_color[0] );
        $rgb2 = hexdec( $split_hex_color[1] );
        $rgb3 = hexdec( $split_hex_color[2] );
        for($i=0;$i<$this->QRCodeNumbers;$i++){
            $qrCode = (new QrCode(uniqid($this->QRPrefix)))
                ->setMargin(5)
                ->useForegroundColor($rgb1,$rgb2,$rgb3);
            $qrCode->writeFile(__DIR__ . "/code.'$i'.png");
            $qrcodes[]=$qrCode->writeDataUri();
        }
        return $qrcodes;
    }
    public function actionPdf() {

        $img="<div>";
        $QRCodes=$this->QRCodesGenerater();
        foreach($QRCodes as  $key=>$code){
          $img.="<img src=\"$code\" alt=\"code\" width=\"500\" height=\"600\">";
        }
        $img.="</div>";
        return $img;
    }
}