<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
?>
<?php $form = ActiveForm::begin(['id' => 'filesForm']); ?>
<?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_wrapper',
    'widgetBody' => '.container-items',
    'widgetItem' => '.item',
    'insertButton' => '.add-item',
    'deleteButton' => '.remove-item',
    'model' => $models[0],
    'formId' => 'filesForm',
    'formFields' => [
        'QRCodeNumbers',
        'QRColor',
        'QRPrefix',
    ],
]); ?>
<?php echo \Yii::$app->session->getFlash('flashMessage'); ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-file"></i> QR Codes
        <button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i> Add New File</button>
        <div class="clearfix"></div>
    </div>
    <div class="panel-body">


        <div class="container-items">
            <?php foreach ($models as $index => $model): ?>
                <div class="item panel panel-default">
                    <div class="panel-heading">
                        <span class="panel-title-address"><?= ($index + 1) ?></span>
                        <button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <?= $form->field($model, "[{$index}]QRCodeNumbers") ?>
                            </div>
                            <div class="col-sm-4">
                                <?= $form->field($model, "[{$index}]QRColor")->input('color') ?>
                            </div>
                            <div class="col-sm-4">
                                <?= $form->field($model, "[{$index}]QRPrefix") ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <?php DynamicFormWidget::end(); ?>
    </div>
</div>
<div class="form-group">
    <?= Html::submitButton('Click To Generate Your Pdfs!!', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>


